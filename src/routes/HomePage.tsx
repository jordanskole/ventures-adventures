import React, { Fragment } from 'react'
import { Row, Col } from 'antd'
import { Helmet } from 'react-helmet'

import { Heading } from '../components/Heading'
import { Text } from '../components/Text'

type HomePageProps = {
  projects: []
}

export const HomePage: React.SFC<HomePageProps> = ({}) => {

  const projects = [
    { link: 'https://mclean.ventures-adventures.com', name: 'McLean by the Numbers'},
    { link: 'https://upccodes.app', name: 'UPC Codes Shopify App'},
    { link: 'https://spacegear.store', name: 'Space Gear'},
    { link: 'https://detroitstartupjobs.com', name: 'Detroit Startup Jobs'},
    { link: 'https://heatherreadphotography.com', name: 'Heather Read Photography'}
  ];

  return (
    <Fragment>
      <Helmet>
        <title>Ventures & Adventures</title>
      </Helmet>
      <Row type='flex' justify='center' align='middle' style={{height: '100%'}}>
        <Col xs={{ span: 22, offset: 1}} lg={{ span: 12, offset: 1, push: 12 }} >
          <Heading h={1} size='xl' fontFamily='Merriweather' bold>
            Ventures &<br />Adventures
          </Heading>
          <Heading h={2} fontFamily='Merriweather' bold>
            by Jordan Skole
          </Heading>
        </Col>
        <Col lg={{span: 11, pull: 12 }}>
          <Heading h={3} fontFamily='Merriweather' align='right' bold>
            Projects:
          </Heading>
          {projects.map(({ link, name }) => {
            return (<a href={link}>
              <Text align='right' color='black' fontFamily='Merriweather' lineHeight={1.8} underline>{name}</Text>
            </a>)
          })}
        </Col>
      </Row>
    </Fragment>
  )
}

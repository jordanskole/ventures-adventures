import React from 'react'

export interface TextProps extends React.CSSProperties {
  align?: "left" | "right" | "center"
  bold?: boolean
  italic?: boolean
  underline?: boolean
  inline?: boolean
  heading?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6'
}

export const Text: React.SFC<TextProps> = ({
  align = "left" as "left",
  bold = false,
  color,
  heading,
  inline = false,
  italic = false,
  fontFamily,
  fontSize = 1.2,
  lineHeight = 1.2,
  underline,
  children
}) => {

  const style: React.CSSProperties = {
    color: color ? color : 'inherit',
    fontWeight: bold ? 'bold' : 'inherit',
    fontStyle: italic ? 'italic' : 'normal',
    fontFamily,
    fontSize: `${fontSize}rem`,
    lineHeight,
    textAlign: align ? align : 'left',
    textDecoration: underline ? 'underline' : 'inherit'
  }

  const Tag = heading
    ? heading
    : inline
      ? 'span'
      : 'div'

  return (<Tag style={style}>{children}</Tag>)
}

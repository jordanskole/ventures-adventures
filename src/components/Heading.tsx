import React from 'react'
import {Text, TextProps} from './Text'

type Heading = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
type HeadingSize = 1 | 1.5 | 2 | 3 | 4 | 6 ;

interface HeadingProps extends TextProps {
  size?: 'xs' | 's' | 'm' | 'lg' | 'xl' | 'xxl'
  h: 1 | 2 | 3 | 4 | 5 | 6
}

export const Heading: React.SFC<HeadingProps> = ({ h = 2, size = 'm', children, ...props }) => {

  const heading: Heading = h === 1
    ? 'h1'
    : h === 2
      ? 'h2'
      : h === 3
        ? 'h3'
        : h === 4
          ? 'h4'
          : h === 5
            ? 'h5'
            : 'h6' // 6

  const fontSize: HeadingSize = size === 'xs'
    ? 1
    : size === 's'
      ? 1.5
      : size === 'm'
        ? 2
        : size === 'lg'
          ? 3
          : size === 'xl'
            ? 4
            : 6 // xxl

  return <Text heading={heading} fontSize={fontSize} {...props}>{children}</Text>
}

import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { HomePage } from './routes/HomePage'

import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
          <Route path="/" exact component={HomePage} />
      </Router>
    );
  }
}

export default App;
